<?php
if(file_exists(get_template_directory().'/inc/enqueue.php')){
require_once(get_template_directory().'/inc/enqueue.php');
}
if(file_exists(get_template_directory().'/theme-options/cs-framework.php')){
require_once(get_template_directory().'/theme-options/cs-framework.php');
}
if(file_exists(get_template_directory().'/inc/theme_support.php')){
require_once(get_template_directory().'/inc/theme_support.php');
}
if(file_exists(get_template_directory().'/inc/wp-bootstrap-navwalker.php')){
require_once(get_template_directory().'/inc/wp-bootstrap-navwalker.php');
}
if(file_exists(get_template_directory().'/inc/comments_style.php')){
require_once(get_template_directory().'/inc/comments_style.php');
}
if(file_exists(get_template_directory().'/inc/custom_breadcrumbs.php')){
require_once(get_template_directory().'/inc/custom_breadcrumbs.php');
}
if(file_exists(get_template_directory().'/inc/custom_widget.php')){
require_once(get_template_directory().'/inc/custom_widget.php');
}
if(file_exists(get_template_directory().'/inc/lib/plugin-activation.php')){
require_once(get_template_directory().'/inc/lib/plugin-activation.php');
}
 if(file_exists(get_template_directory().'/inc/theme-options.php')) {
	require_once(get_template_directory().'/inc/theme-options.php');
} 
if(file_exists(get_template_directory().'/inc/flaticon.php')){
	require_once(get_template_directory().'/inc/flaticon.php');
}
 if(file_exists(get_template_directory().'/inc/demo-importer.php')) {
	require_once(get_template_directory().'/inc/demo-importer.php');
}
 if(file_exists(get_template_directory().'/inc/custom_color.php')) {
	require_once(get_template_directory().'/inc/custom_color.php');
}
/*	- This Is For Admin Header Style
=======================================================*/
function portfolio_admin_style() {
		wp_enqueue_style(
			'portfolio-admin-css',
			get_template_directory_uri() . '/assets/css/custom_script.css'
		);
			$custom_css = "
							@media screen and (max-width: 782px){
							#wpadminbar #wp-admin-bar-wp-logo > .ab-item .ab-icon,#wpadminbar .ab-icon, #wpadminbar .ab-item:before, #wpadminbar>#wp-toolbar>#wp-admin-bar-root-default .ab-icon {
								height: 19px !important;
								}
							html #wpadminbar {
									height: 46px;
									min-width: 100%;
								}
								}
							@media screen and  (max-width: 600px){
								#wpadminbar {
									position: fixed!important;
									}
								}
							@media screen and (max-width: 840px){
								#wpadminbar, #wpadminbar * {
									font-size: 12px;
									font-weight: 400;
									line-height: 32px;
								}
								}	
							
								";
    wp_add_inline_style( 'portfolio-admin-css', $custom_css );
}
add_action( 'wp_enqueue_scripts', 'portfolio_admin_style' );
//for search
function portfolio_search_filter($query) {
if ($query->is_search) {
$query->set('post_type', 'post');
}
return $query;
}
add_filter('pre_get_posts','portfolio_search_filter');